//
//  MapViewController.h
//  PhotoMap
//
//  Created by Michael Mangold on 4/16/12.
//  Copyright (c) 2012 Michael Mangold. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class MapViewController;
@protocol MapViewControllerDelegate <NSObject>

- (UIImage *)mapViewController:(MapViewController *)sender imageForAnnotation:(id <MKAnnotation>)annotation;
@optional - (void)photosTableViewController:(id *)sender chosePhoto:(id)photo;

@end

@interface MapViewController : UIViewController <UINavigationBarDelegate>
@property (nonatomic, strong) NSArray *annotations; // of id <MKAnnotation>
@property (nonatomic, weak) id <MapViewControllerDelegate> delegate;
@end
