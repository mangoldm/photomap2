//
//  PlacesTableViewController.m
//  PhotoMap
//
//  Created by Michael Mangold on 2/24/12.
//  Copyright (c) 2012 Michael Mangold. All rights reserved.
//

#import "PlacesTableViewController.h"
#import "FlickrFetcher.h"
#import "PhotosTableViewController.h"
#import "MapViewController.h"
#import "FlickrPhotoAnnotation.h"

@interface PlacesTableViewController () <MapViewControllerDelegate>

@end

@implementation PlacesTableViewController

@synthesize places = _places;

- (void)setPlaces:(NSArray *)places
{
	if (_places != places) {
		_places = places;
		[self.tableView reloadData];
	}
}

- (IBAction)refresh:(id)sender
{
	// Create spinning 'wait' indicator
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[spinner startAnimating];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner];
	
	// create GCD queue then dispatch
	dispatch_queue_t downloadQueue = dispatch_queue_create("flickr downloader", NULL);
	dispatch_async(downloadQueue, ^{
		NSArray *placesArray = [FlickrFetcher topPlaces];
		
		// Sort topPlaces
		NSSortDescriptor *placeName = [[NSSortDescriptor alloc] initWithKey:FLICKR_PLACE_NAME ascending:YES];
		NSArray	   *sortDescriptors = [NSArray arrayWithObjects:placeName, nil];
		NSArray	 *sortedPlacesArray = [placesArray sortedArrayUsingDescriptors:sortDescriptors];
		
		// keep UI processing on main thread
		dispatch_async(dispatch_get_main_queue(), ^{
			self.navigationItem.rightBarButtonItem = sender; // Turn off spinning indicator
			self.places	= sortedPlacesArray;
		});
	});
	dispatch_release(downloadQueue);
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    self.tabBarController.delegate = self;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.places count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Flickr Place";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary   *place = [self.places objectAtIndex:indexPath.row];
	NSString   *tempPlace = [place objectForKey:FLICKR_PLACE_NAME];
	NSArray *parsedPlaces = [tempPlace componentsSeparatedByString:@","];
	NSString        *city = [parsedPlaces	objectAtIndex:0];
	NSString      *region = @"No Region";
	NSString  *tempRegion = nil;
	
	// Build the region string (e.g. Washington, DC, United States)
	int  parsedPlacesCount = [parsedPlaces count];
	for (int i = 1; i <= parsedPlacesCount; i++) {
		tempRegion = [parsedPlaces objectAtIndex:i - 1];
		if (i == 1) region = tempRegion;
		else {
			region = [region stringByAppendingString:@","];
			region = [region stringByAppendingString:tempRegion];
		}
	}
	
	cell.textLabel.text = city;
	cell.detailTextLabel.text = region;
    
    return cell;
}

- (NSArray *)photosForPlace:(id)place;
{
	NSString *tempPlaceName	  = [place objectForKey:FLICKR_PLACE_NAME];
	NSArray	 *parsedPlaces	  = [tempPlaceName componentsSeparatedByString:@","];
	NSString *placeName		  = [parsedPlaces objectAtIndex:0];
	self.navigationItem.title = placeName;
	
    // get 50 recent photos for the city
	NSArray *tempPhotos = [FlickrFetcher photosInPlace:place maxResults:50];
	return tempPhotos;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"Show Photos"])
    {
		NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
		id place = [self.places objectAtIndex:indexPath.row];
		
		// Create spinning 'wait' indicator
		UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		[cell addSubview:spinner];
		spinner.frame = CGRectMake(0, 0, 24, 24);
		cell.accessoryView = spinner;
		[spinner startAnimating];
		
        // Get reference to the destination view controller and pass the list of photos
		__block NSArray *tempArray;
        PhotosTableViewController *photosTableViewController = [segue destinationViewController];
		dispatch_queue_t photosQueue = dispatch_queue_create("photos downloader", NULL);
		dispatch_async(photosQueue, ^{
			tempArray = [self photosForPlace: place];
			// Send photos to the main thread
			dispatch_async(dispatch_get_main_queue(),^{
				photosTableViewController.photos = tempArray;
			});
		});
		dispatch_release(photosQueue);
		[spinner stopAnimating]; // turn off spinning wait indicator
		cell.accessoryView = nil; // restore accessoryView
    }
}

// Sets places as map annotations
- (NSArray *) mapAnnotations
{
    NSMutableArray *annotations = [NSMutableArray arrayWithCapacity:[self.places count]];
    for (NSDictionary *place in self.places) {
        [annotations addObject:[FlickrPhotoAnnotation annotationForPhoto:place]];
    }
    return annotations;
}

- (UIImage *)mapViewController:(MapViewController *)sender imageForAnnotation:(id<MKAnnotation>)annotation
{
    FlickrPhotoAnnotation *fpa = (FlickrPhotoAnnotation *)annotation;
    NSURL *url = [FlickrFetcher urlForPhoto:fpa.photo format:FlickrPhotoFormatSquare];
    NSData *data = [NSData dataWithContentsOfURL:url];
    return data ? [UIImage imageWithData:data] : nil;
}

#pragma mark - TabBarController delegate

// Called when user selects a tab in the tab bar
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if ([viewController.title isEqualToString:@"Map"]) {
        MapViewController *mapVC = [self.tabBarController.viewControllers objectAtIndex:1];
        mapVC.annotations = [self mapAnnotations];
        mapVC.delegate = self;
    }
}

@end
