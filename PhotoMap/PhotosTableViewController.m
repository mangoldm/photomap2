//
//  PhotosTableViewController.m
//  PhotoMap
//
//  Created by Michael Mangold on 2/24/12.
//  Copyright (c) 2012 Michael Mangold. All rights reserved.
//

#import "PhotosTableViewController.h"
#import "FlickrFetcher.h"
#import "PlacesTableViewController.h"
#import "FlickrPhotoAnnotation.h"

@interface PhotosTableViewController () <MapViewControllerDelegate>

@end

@implementation PhotosTableViewController

@synthesize spinner     = _spinner;
@synthesize photos      = _photos;
@synthesize chosenPlace = _chosenPlace;
@synthesize delegate    = _delegate;

#define RECENT_PHOTOS_KEY @"ScrollingPhotoViewController.Recent"

- (IBAction)refresh:(id)sender {
	
	// Create spinning 'wait' indicator
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[spinner startAnimating];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner];
	// create GCD queue then dispatch
	dispatch_queue_t defaultsQueue = dispatch_queue_create("defaults fetcher", NULL);
	dispatch_async(defaultsQueue, ^{
		NSArray *tempArray = [[NSUserDefaults standardUserDefaults] objectForKey:RECENT_PHOTOS_KEY];
		// send defaults to main thread
		dispatch_async(dispatch_get_main_queue(), ^{
			// Reverse photos array so the most recently-viewed is on top
			self.photos = [[tempArray reverseObjectEnumerator] allObjects];
		});
	});
	dispatch_release(defaultsQueue);
	// Turn off spinning indicator
	self.navigationItem.rightBarButtonItem = sender; 
}

- (void)setChosenPlace:(id)chosenPlace
{
	if (_chosenPlace != chosenPlace) {
		_chosenPlace = chosenPlace;
	}
}

- (void)setPhotos:(NSArray *)photos
{
	if (_photos != photos) {
		_photos = photos;
		[self.tableView reloadData];
		[self.spinner stopAnimating];
	}
}

- (void)didReceiveMemoryWarning
{
    //	Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    self.tabBarController.delegate = self;
}

- (void)viewDidUnload
{
	[self setSpinner:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = DEFAULT_COLOR;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.photos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Photo Row";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
	NSDictionary *photo = [self.photos objectAtIndex:indexPath.row];
	NSString *tempTitle = [photo objectForKey:FLICKR_PHOTO_TITLE];
	
	// Limit Title to 30 characters
	if (tempTitle.length > 30) {
		NSString *truncatedTitle = [tempTitle substringToIndex:30];
		tempTitle = truncatedTitle;
	}
	
	NSString *tempDescription = [photo valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
	
	// Limit Description to 40 characters
	if (tempDescription.length > 40) {
		NSString *truncatedDescription = [tempDescription substringToIndex:40];
		tempDescription = truncatedDescription;
	}
	
	// Assign cell labels
	if (![tempTitle isEqualToString:@""])
		cell.textLabel.text = tempTitle;
	else
		if (![tempDescription isEqualToString:@""])
			cell.textLabel.text = tempDescription;
		else
			cell.textLabel.text = @"Unknown";
	if ([tempDescription isEqualToString:@""])
		cell.detailTextLabel.text = @"No Description";
	else
		cell.detailTextLabel.text = tempDescription;
    
    // Prefix cell label with row number
    NSString *currentRow = [NSString stringWithFormat:@"%d: ",indexPath.row + 1];
    cell.textLabel.text = [currentRow stringByAppendingString: cell.textLabel.text];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{	
	// send chosen photo to delegate
	id photo = [self.photos objectAtIndex:indexPath.row];
	[self.delegate photosTableViewController:self chosePhoto:photo];
}

#pragma mark - TabBarController delegate


// Sets places as map annotations
- (NSArray *) mapAnnotations
{
    NSMutableArray *annotations = [NSMutableArray arrayWithCapacity:[self.photos count]];
    for (NSDictionary *photo in self.photos) {
        [annotations addObject:[FlickrPhotoAnnotation annotationForPhoto:photo]];
    }
    return annotations;
}

- (UIImage *)mapViewController:(MapViewController *)sender imageForAnnotation:(id<MKAnnotation>)annotation
{
    FlickrPhotoAnnotation *fpa = (FlickrPhotoAnnotation *)annotation;
    NSURL *url = [FlickrFetcher urlForPhoto:fpa.photo format:FlickrPhotoFormatSquare];
    NSData *data = [NSData dataWithContentsOfURL:url];
    return data ? [UIImage imageWithData:data] : nil;
}

#pragma mark - TabBarController delegate

// Called when user selects a tab in the tab bar
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if ([viewController.title isEqualToString:@"Map"]) {
        MapViewController *mapVC = [self.tabBarController.viewControllers objectAtIndex:1];
        mapVC.annotations = [self mapAnnotations];
        mapVC.delegate = self;
    }
}

@end
